#ifndef UTILS
#define UTILS

#include <opencv2/opencv.hpp>

struct color_interval_t
{
    cv::Scalar min, max;
};

#endif // UTILS

