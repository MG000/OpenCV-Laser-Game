#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include "manager.h"

int main(int argc, char **argv)
{
    Manager manager(argc, argv);
    manager.runCircle();
}
