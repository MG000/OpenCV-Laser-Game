#ifndef MANAGER_H
#define MANAGER_H

#include <visor.h>
#include <gamewindow.h>

class Manager
{
public:
    Manager(int argc, char *argv[]);
    void runCircle();

private:
    GameWindow gameWindow_;
    Visor visor_;
};



#endif // MANAGER_H
