#ifndef SELECTOR_H
#define SELECTOR_H

#include <opencv2/opencv.hpp>
#include <memory>

//Object for select color in interval, from cv::Mat (image)

class Selector
{
public:
    Selector(const std::string& selector_name, const std::shared_ptr<cv::Mat>& resurs,
             const cv::Vec3i& min = {0, 0, 0}, const cv::Vec3i& max = {255, 255, 255});
    Selector(const Selector&);
    Selector& operator=(const Selector&);

    ~Selector();
    void open();
    void close();
    void update();

    cv::Scalar getMin() const;
    cv::Scalar getMax() const;
    std::shared_ptr<const cv::Mat> getMat() const;

private:
    std::string name_;
    std::shared_ptr<cv::Mat> resurs_;
    std::shared_ptr<cv::Mat> result_;
    cv::Vec3i min_, max_;
    bool isOpen_;

    void swap(Selector&);
};
#endif // SELECTOR_H
