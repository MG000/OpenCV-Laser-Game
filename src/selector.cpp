#include "selector.h"
#include <iostream>
#include <utility>

Selector::Selector(const std::__cxx11::string &selector_name, const std::shared_ptr<cv::Mat> &resurs, const cv::Vec3i &min, const cv::Vec3i &max):
    name_(selector_name),
    resurs_(resurs),
    result_(std::make_shared<cv::Mat>()),
    min_(min),
    max_(max),
    isOpen_(false)
{}

Selector::Selector(const Selector& selector):
    name_(selector.name_),
    resurs_(selector.resurs_),
    result_(std::make_shared<cv::Mat>(*selector.result_)),
    min_(selector.min_),
    max_(selector.max_),
    isOpen_(false)
{}

Selector& Selector::operator=(const Selector& selector)
{
    Selector tmp(selector);
    Selector::swap(tmp);
    return *this;
}

Selector::~Selector()
{
    if(isOpen_)
    {
        Selector::close();
    }
}

void Selector::open()
{
    if(!isOpen_)
    {
        cv::namedWindow(name_);
        cv::createTrackbar("minR", name_, &min_[2], 255);
        cv::createTrackbar("minG", name_, &min_[1], 255);
        cv::createTrackbar("minB", name_, &min_[0], 255);
        cv::createTrackbar("maxR", name_, &max_[2], 255);
        cv::createTrackbar("maxG", name_, &max_[1], 255);
        cv::createTrackbar("maxB", name_, &max_[0], 255);
        isOpen_ = true;
    }
}

void Selector::close()
{
    if(isOpen_)
    {
        cv::destroyWindow(name_);
    }
}

void Selector::update()
{
    cv::inRange(*resurs_, min_, max_, *result_);

    if(isOpen_)
    {
        cv::imshow(name_, *result_);
    }
}

cv::Scalar Selector::getMin() const
{
    return min_;
}

cv::Scalar Selector::getMax() const
{
    return max_;
}

std::shared_ptr<const cv::Mat> Selector::getMat() const
{
    return result_;
}

void Selector::swap(Selector& selector)
{
    std::swap(name_, selector.name_);
    std::swap(resurs_, selector.resurs_);
    std::swap(result_, selector.result_);
    std::swap(min_, selector.min_);
    std::swap(max_, selector.max_);
    //isOpen_ stay
}

