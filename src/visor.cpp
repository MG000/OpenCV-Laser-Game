#include "visor.h"

#include <memory>
#include <vector>
#include <ctime>
#include <thread>

#include "opencv2/opencv.hpp"
#include "repeater.h"
#include "selector.h"
#include "constants.h"

Visor::Visor():
    state_(DOWN),
    continueWork_(false)
{}

void Visor::monitor(std::mutex* loc)
{
    cv::VideoCapture cap(0);
    std::shared_ptr<cv::Mat> img(std::make_shared<cv::Mat>());

    cap >> *img;

    std::vector<cv::Vec3f> circles;

    auto searchCircles = [&img, &circles] () -> void
        {
            cv::Mat img_gray;
            cv::cvtColor(*img, img_gray, CV_BGR2GRAY);
            circles.clear();
            cv::HoughCircles(img_gray, circles, CV_HOUGH_GRADIENT, Constants::DP, img_gray.rows / Constants::DEVIDER_FOR_MIN_DISTANCE,
                             100, 100, Constants::MIN_RADIUS, Constants::MIN_RADIUS);
        };
    searchCircles();
    Repeater<decltype(searchCircles)> circleFinder(searchCircles, Constants::COUNT_REPEAT_EVERY_TIMES_FIND_CIRCLES);

    std::shared_ptr<Selector> bgr(std::make_shared<Selector>(Selector("bgr", img))); // СДЕСЬ НАДО ДОБАВИТЬ ГРАНИЦЫ!!!

//    bgr->open();
    time_t prevInCircle(time(nullptr));

    continueWork_ = true;
    std::chrono::microseconds cadr(1000);
    while(continueWork_)
    {
      //  cv::imshow("main", *img);
        std::cout << state_ << std::endl;
        cap >> *img;
        circleFinder.takeStep();

        bgr->update();

        if(circles.empty())
        {
            state_ = CANTFINDCIRCLE;
            std::this_thread::sleep_for(cadr);
            //cv::waitKey(33);
            continue;
        }

        cv::Vec3f circle = circles.front();
        cv::Rect roi(circle[0] - circle[2], circle[1] - circle[2], circle[2] * 2, circle[2] * 2);

        if(!(0 <= roi.x && 0 <= roi.width && roi.x + roi.width <= img->cols && 0 <= roi.y && 0 <= roi.height && roi.y + roi.height <= img->rows))
        {
            std::this_thread::sleep_for(cadr);
            //cv::waitKey(33);
            continue;
        }

        cv::rectangle(*img, roi, Constants::GREEN);

        if(cv::sum((*bgr->getMat())(roi)) == Constants::EMPTY)
        {
            state_ = NOTINCIRCLE;
            prevInCircle = time(nullptr);
        }
        else
        {
            if((state_ == INCIRCLE || state_ == REDYFORMOVE) && (time(nullptr) - prevInCircle >= Constants::WAIT_TIME))
            {
                state_ = REDYFORMOVE;
            }
            else
            {
                state_ = INCIRCLE;
            }
        }
        std::this_thread::sleep_for(cadr);
        //cv::waitKey(33);
    }
   // bgr->close();
    state_ = DOWN;
}

void Visor::down()
{
    continueWork_ = false;
}

Visor::StateOfVisor Visor::getState() const
{
    return state_;
}
