#ifndef CONSTANTS
#define CONSTANTS

#include <cstdlib>
#include "opencv2/opencv.hpp"

namespace Constants
{

    constexpr int START_POS_CIRCLE_X = 200;
    constexpr int START_POS_CIRCLE_Y = 200;
    constexpr size_t CIRCLE_RADIUS = 20;
    const cv::Scalar CIRCLE_COLOR(0, 0, 0);
    const int CIRCLE_THICKNESS = 3;

    const std::string GAMEWINDOW_NAME = "GameWindow";
    constexpr size_t WIDTH_WINDOW = 1920;
    constexpr size_t HEIGHT_WINDOW = 1080;
    const cv::Scalar START_WINDOW_COLOR(255, 255, 255);
    const cv::Scalar LOSSE_COLOR(0, 0, 255);
    const cv::Scalar WHITE(255, 255, 255);

    const cv::Scalar MIN_BORDER(16, 1, 91);
    const cv::Scalar MAX_BORDER(100, 75, 255);
    constexpr size_t COUNT_REPEAT_EVERY_TIMES_FIND_CIRCLES = 5;
    constexpr int DP = 2;
    constexpr int DEVIDER_FOR_MIN_DISTANCE = 8;
    constexpr int MIN_RADIUS = 10;
    constexpr int MAX_RADIUS = 100;
    const cv::Scalar EMPTY(0, 0, 0);
    const cv::Scalar GREEN(0, 255, 0);
    constexpr size_t WAIT_TIME = 3;
}

#endif // CONSTANTS

