#include "communicationnode.h"

std::map<std::string, std::queue<std::string>&> CommunicationNode::mapOfNodes;

CommunicationNode::CommunicationNode(const std::string &name):
    nameNode_(name),
    pullMessages_()
{
    if(!mapOfNodes.insert(connection(name, pullMessages_)).second)
    {
        throw std::invalid_argument("already exists node with this name");
    }
}

CommunicationNode::~CommunicationNode()
{
    mapOfNodes.erase(nameNode_);
}

void CommunicationNode::sendMessage(const std::string &nameReceivingNode, const std::string &message)
{
    mapOfNodes.at(nameReceivingNode).push(message);
}

std::string CommunicationNode::getMessage() const
{
    if(pullMessages_.empty())
    {
       return std::string();
    }
    return pullMessages_.front();
}

void CommunicationNode::popMessage()
{
    pullMessages_.pop();
}

bool CommunicationNode::checkExistenceNode(const std::string &nameNode)
{
    return mapOfNodes.find(nameNode) != mapOfNodes.end();
}

