TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    communicationnode.cpp \
    gamewindow.cpp \
    manager.cpp \
    selector.cpp \
    visor.cpp \
    utils.cpp


#QMAKE_LIBS+=-static -lgomp -lpthread
#QMAKE_CXXFLAGS+=-msse3 -fopenmp
#QMAKE_CXXFLAGS+=-U_WIN32
QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread

INCLUDEPATH += .
INCLUDEPATH += /usr/local/include
INCLUDEPATH += /usr/local/include/opencv
INCLUDEPATH += /usr/local/include/opencv2
INCLUDEPATH += /usr/local/include/opencv2/core
INCLUDEPATH += /usr/local/include/opencv2/highgui
INCLUDEPATH += /usr/local/include/opencv2/imgproc
INCLUDEPATH += /usr/local/include/opencv2/flann
INCLUDEPATH += /usr/local/include/opencv2/photo
INCLUDEPATH += /usr/local/include/opencv2/video
INCLUDEPATH += /usr/local/include/opencv2/features2d
INCLUDEPATH += /usr/local/include/opencv2/objdetect
INCLUDEPATH += /usr/local/include/opencv2/calib3d
INCLUDEPATH += /usr/local/include/opencv2/ml
INCLUDEPATH += /usr/local/include/opencv2/contrib
LIBS += `pkg-config opencv --cflags --libs`

HEADERS += \
    communicationnode.h \
    gamewindow.h \
    manager.h \
    repeater.h \
    selector.h \
    visor.h \
    utils.h \
    constants.h

