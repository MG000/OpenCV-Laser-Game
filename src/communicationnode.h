#ifndef COMMUNICATIONCENTRE_H
#define COMMUNICATIONCENTRE_H

#include <vector>
#include <string>
#include <queue>
#include <unordered_map>
#include <map>

class CommunicationNode
{
public:
    CommunicationNode(const std::string& name);
    ~CommunicationNode();

    CommunicationNode(const CommunicationNode&) = delete;
    CommunicationNode& operator=(const CommunicationNode&) = delete;

    CommunicationNode(CommunicationNode&&) = default;
    CommunicationNode& operator=(CommunicationNode&&) = default;

    void sendMessage(const std::string& nameReceivingNode, const std::string& message);
    std::string getMessage() const;
    void popMessage();

    static bool checkExistenceNode(const std::string& nameNode);

private:

    std::string nameNode_;
    std::queue<std::string> pullMessages_;

    using connection = std::pair<std::string, std::queue<std::string>&>;
    static std::map<std::string, std::queue<std::string>&> mapOfNodes;
};

#endif // COMMUNICATIONCENTRE_H
