#ifndef VISOR_H
#define VISOR_H

#include <mutex>

class Visor
{
public:

    enum StateOfVisor
    {
        DOWN,
        CANTFINDCIRCLE,
        NOTINCIRCLE,
        INCIRCLE,
        REDYFORMOVE
    };

    Visor();
    void monitor(std::mutex* loc = nullptr);
    void down();
    StateOfVisor getState() const;

private:
    StateOfVisor state_;
    volatile bool continueWork_;
};

#endif // VISOR_H
