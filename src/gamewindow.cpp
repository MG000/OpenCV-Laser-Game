#include "gamewindow.h"
#include <ctime>
#include <thread>
#include <iostream>

#include "constants.h"

GameWindow::GameWindow(GameWindow::StateOfDisplay state):
    state_(state),
    continueRun_(false)
{}

void GameWindow::setState(GameWindow::StateOfDisplay state)
{
    state_ = state;
}

GameWindow::StateOfDisplay GameWindow::getState() const
{
    return state_;
}

void GameWindow::run(std::mutex* loc, int newPatametr)
{
    bool continueRun_ = true;

    cv::Mat field(Constants::WIDTH_WINDOW, Constants::HEIGHT_WINDOW, CV_2PI, Constants::START_WINDOW_COLOR);

    struct {
        int x, y;
        size_t r;
        cv::Scalar color;
        int thickness;
    } circle = {Constants::START_POS_CIRCLE_X, Constants::START_POS_CIRCLE_Y, Constants::CIRCLE_RADIUS,
                Constants::CIRCLE_COLOR, Constants::CIRCLE_THICKNESS};

    cv::circle(field, cv::Point(circle.x, circle.y), circle.r, circle.color, circle.thickness);

    cv::imshow(Constants::GAMEWINDOW_NAME, field);

    cv::Point target(GameWindow::generationTarget(circle.r));
    while(continueRun_)
    {
        switch (state_)
        {
        case WAITING:
            break;
        case MOVECIRCLE:
            cv::circle(field, cv::Point(circle.x, circle.y), circle.r, Constants::WHITE, circle.thickness);
            if((circle.x == target.x) && (circle.y == target.y))
            {
                target = GameWindow::generationTarget(circle.r);
            }
            else
            {
                if(circle.x != target.x)
                {
                    circle.x += (circle.x > target.x ? -1 : 1);
                }
                if(circle.y != target.y)
                {
                    circle.y += (circle.y > target.y ? -1 : 1);
                }
            }
            cv::circle(field, cv::Point(circle.x, circle.y), circle.r, circle.color, circle.thickness);
            break;
        case GAMEOVER:
            field = cv::Mat(Constants::WIDTH_WINDOW, Constants::HEIGHT_WINDOW, CV_32FC3, Constants::LOSSE_COLOR);
            break;
        }
        cv::imshow(Constants::GAMEWINDOW_NAME, field);
        cv::waitKey(33);
    }
}

void GameWindow::stop()
{
    continueRun_ = false;
}

cv::Point GameWindow::generationTarget(size_t limitation) const
{
    return cv::Point((rand() % (Constants::HEIGHT_WINDOW - 2 * limitation)) + limitation,
                     (rand() % (Constants::WIDTH_WINDOW - 2 * limitation)) + limitation);
}
