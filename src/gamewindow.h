#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "opencv2/opencv.hpp"

#include "communicationnode.h"

#include <mutex>

class GameWindow
{
public:

    enum StateOfDisplay
    {
        WAITING,
        MOVECIRCLE,
        GAMEOVER,
    };

    GameWindow(GameWindow::StateOfDisplay);
    
    void setState(GameWindow::StateOfDisplay);
    GameWindow::StateOfDisplay getState() const;
    void run(std::mutex* loc = nullptr, int newParameter = 100);
    void stop();

private:

    volatile GameWindow::StateOfDisplay state_;
    volatile bool continueRun_;
    cv::Mat field_;
    cv::Point generationTarget(size_t limit) const;
};

#endif // GAMEWINDOW_H
