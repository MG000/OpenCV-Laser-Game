#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
  if(argc != 3)
  {
    return 1;
  }

  std::cout << " SUM : " << std::atoi(argv[1]) + std::atoi(argv[2]) << "\n";
  return 0;
}
